//
// Created by Mikita Kalavaitsis on 04.03.2021.
//

import Foundation

class DIContainer {
    private static let _container = DIContainer()
    private final var definedTypes: Dictionary<String, (DIContainer) -> AnyObject>;

    private init() {
        definedTypes = Dictionary<String, (DIContainer) -> AnyObject>()
    }

    class func defineSingle<T: AnyObject>(definition: (DIContainer) -> T) {
        let name = NSStringFromClass(T.self)
        var instance: T
        _container.definedTypes[name] = { container in
            if (instance == nil) {
                instance = definition(container)
            }
            return instance
        }
    }

    class func defineFabric<T: AnyObject>(definition: @escaping (DIContainer) -> T) {
        let name = NSStringFromClass(T.self)
        _container.definedTypes[name] = definition
    }

    class func get<T: AnyObject>() -> T? {
        let definition = _container.definedTypes[NSStringFromClass(T.self)]
        return definition?(_container) as? T
    }
}
