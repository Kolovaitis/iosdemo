
import Foundation
import Combine
import SwiftUI

class AuthorsStore: ObservableObject {
    private var areLoaded = false
    @Published var authors: [Author] = []
    
    private var playersService: PlayersService
    
    init(playersService: PlayersService) {
        self.playersService = playersService
    }
    
    func loadAll() {
        if !areLoaded {
            areLoaded = true
            print("Loading players")
            playersService.loadAllPlayers { players, error in
                DispatchQueue.main.async {
                    self.authors = players
                }
            }
        }
    }
    
    func resetAreLoaded() {
        areLoaded = false
    }
}

//For preview

func getLoadedStore() -> AuthorsStore {
    let playersStore = AuthorsStore(playersService: ArrayPlayersService())
    playersStore.loadAll()
    return playersStore
}

