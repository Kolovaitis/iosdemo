//
//  MapView.swift
//  MinecraftPlayers
//
//  Created by Ilyat on 2/21/21.
//  Copyright © 2021 Ilyat. All rights reserved.
//

import SwiftUI
import YandexMapsMobile

fileprivate struct MapPlayerIcon: View {
    var player: Author

    var body: some View {
        VStack(spacing: 5) {
            Text(player.nickname)
                    .padding(2)
                    .foregroundColor(Color(UIColor.systemBackground))
                    .colorInvert()
                    .background(Color.gray.opacity(0.3))
            player.avatarImage
                    .interpolation(.none)
                    .resizable()
                    .cornerRadius(3)
                    .frame(width: 25, height: 25)
        }
    }
}

struct MapView: UIViewRepresentable {
    @EnvironmentObject private var playersStore: AuthorsStore

    let DRAGGABLE_PLACEMARK_CENTER = YMKPoint(latitude: 59.948, longitude: 30.323)
    let OBJECT_SIZE: Double = 0.0015

    func makeUIView(context: Context) -> YMKMapView {
        let map = YMKMapView()
        playersStore.authors.forEach { author in
            createMapObjects(view: map, author)
        }
        return map
    }

    func createMapObjects(view: YMKMapView, _ author: Author) {
        let mapObjects = view.mapWindow.map.mapObjects

        var point = YMKPoint(latitude: author.location?.latitude ?? 53, longitude: author.location?.longitude ?? 27)

        var provider = YRTViewProvider(uiView: UIView())
        let placemark = mapObjects.addPlacemark(with: point, view: provider!)
        placemark.setIconWith(author.avatarImage.asUIImage())
    }


//    func createPlacemarkMapObjectWithViewProvider(view: YMKMapView) {
//        let view =
//
//        let viewProvider = YRTViewProvider(uiView: textView);
//
//        let mapObjects = view.mapWindow.map.mapObjects;
//        let viewPlacemark = mapObjects.addPlacemark(
//                with: YMKPoint(latitude: 59.946263, longitude: 30.315181),
//                view: viewProvider!);
//    }

    func updateUIView(_ uiView: YMKMapView, context: Context) {
    }
}

extension View {
// This function changes our View to UIView, then calls another function
// to convert the newly-made UIView to a UIImage.
    public func asUIImage() -> UIImage {
        let controller = UIHostingController(rootView: self)

        controller.view.frame = CGRect(x: 0, y: CGFloat(Int.max), width: 1, height: 1)
        UIApplication.shared.windows.first!.rootViewController?.view.addSubview(controller.view)

        let size = controller.sizeThatFits(in: UIScreen.main.bounds.size)
        controller.view.bounds = CGRect(origin: .zero, size: size)
        controller.view.sizeToFit()

// here is the call to the function that converts UIView to UIImage: `.asImage()`
        let image = controller.view.asUIImage()
        controller.view.removeFromSuperview()
        return image
    }
}

extension UIView {
// This is the function to convert UIView to UIImage
    public func asUIImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
}