//
// Created by Mikita Kalavaitsis on 08.03.2021.
//

import SwiftUI

struct Main: View {

    var body: some View {

        VStack {

            Text("Home")
            Button(action: {
                UserDefaults.standard.set(false, forKey: "status")
                NotificationCenter.default.post(name: NSNotification.Name("statusChange"), object: nil)

            }) {

                Text("Logout")
            }
        }
    }
}
